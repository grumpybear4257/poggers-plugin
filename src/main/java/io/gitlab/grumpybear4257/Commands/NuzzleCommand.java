package io.gitlab.grumpybear4257.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class NuzzleCommand implements CommandExecutor {
    private static final Random rng = new Random();

    private static final List<String> options = Arrays.asList(
            "{player1} nuzzles {player2}",
            "{player1} notices {player2}'s bulge OwO",
            "{player1} gave {player2} a big ol' smoochie",
            "{player1} gave {player2} a big ol' smoochie on the coochie",
            "{player1} snuggles up closer to {player2} uwu",
            "{player1} pats {player2} :3",
            "{player1} tried to be lewd to {player2}, but it was kinda cringe ngl"
    );

    private static String getMessage(String sender, String receiver) {
        String message = options.get(rng.nextInt(options.size()));
        message = message.replace("{player1}", ChatColor.AQUA + sender + ChatColor.GOLD);
        message = message.replace("{player2}", ChatColor.AQUA + receiver + ChatColor.GOLD);

        return message;
    }

    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if (!sender.hasPermission("ep.nuzzle")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use that command!");
            return true;
        }

        if (args.length != 1)
            return false;

        if (args[0].equalsIgnoreCase("all")) {
            for (Player player : Bukkit.getOnlinePlayers())
                Bukkit.broadcastMessage(getMessage(sender.getName(), player.getName()));

            return true;
        }

        Player nuzzled = Bukkit.getPlayerExact(args[0]);
        if (nuzzled == null) {
            sender.sendMessage(ChatColor.RED + "Cannot find player with name: " + ChatColor.WHITE + args[0]);
            return true;
        }

        Bukkit.broadcastMessage(getMessage(sender.getName(), nuzzled.getName()));
        return true;
    }
}
