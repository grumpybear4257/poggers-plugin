package io.gitlab.grumpybear4257.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class BruhCommand implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if (!sender.hasPermission("ep.bruh")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use that command!");
            return true;
        }

        Bukkit.broadcastMessage(ChatColor.GOLD + "This really do be a bruh moment.");
        return true;
    }
}
