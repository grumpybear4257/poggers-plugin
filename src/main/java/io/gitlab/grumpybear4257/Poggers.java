package io.gitlab.grumpybear4257;

import io.gitlab.grumpybear4257.Commands.*;
import org.bukkit.plugin.java.JavaPlugin;

public class Poggers extends JavaPlugin {
    @Override
    public void onEnable() {
        getLogger().info("Registering Commands");
        this.getCommand("pog").setExecutor(new PogCommand());
        this.getCommand("rt").setExecutor(new RetweetCommand());
        this.getCommand("ok").setExecutor(new OkJonCommand());
        this.getCommand("babyrage").setExecutor(new BabyRageCommand());
        this.getCommand("l").setExecutor(new LCommand());
        this.getCommand("f").setExecutor(new FCommand());
        this.getCommand("bean").setExecutor(new BeanCommand());
        this.getCommand("cringe").setExecutor(new CringeCommand());
        this.getCommand("bruh").setExecutor(new BruhCommand());
        this.getCommand("nuzzle").setExecutor(new NuzzleCommand());

        getLogger().info("Registering Listeners");
        getServer().getPluginManager().registerEvents(new NewMessageListener(), this);
    }
}
