package io.gitlab.grumpybear4257;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class NewMessageListener implements Listener {
    private static String lastChatPlayer;
    private static String lastChatMessage;

    @EventHandler
    public void onNewMessage(AsyncPlayerChatEvent event) {
        lastChatPlayer = event.getPlayer().getName();
        lastChatMessage = event.getMessage();
    }

    public static String getLastChatPlayer() {
        return lastChatPlayer;
    }

    public static String getLastChatMessage() {
        return lastChatMessage;
    }
}
