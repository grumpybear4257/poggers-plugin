package io.gitlab.grumpybear4257.Commands;

import io.gitlab.grumpybear4257.NewMessageListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class RetweetCommand implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if (!sender.hasPermission("ep.rt")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use that command!");
            return true;
        }

        Bukkit.broadcastMessage(ChatColor.AQUA + sender.getName() + ChatColor.GOLD + " retweeted " + ChatColor.AQUA +
                NewMessageListener.getLastChatPlayer() + ChatColor.GOLD +  " - " + ChatColor.GREEN +
                NewMessageListener.getLastChatMessage());
        return true;
    }
}
