package io.gitlab.grumpybear4257.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CringeCommand implements CommandExecutor {
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if (!sender.hasPermission("ep.cringe")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use that command!");
            return true;
        }

        if (args.length != 1)
            return false;

        Player cringe = Bukkit.getPlayerExact(args[0]);
        if (cringe == null) {
            sender.sendMessage(ChatColor.RED + "Cannot find player with name: " + ChatColor.WHITE + args[0]);
            return true;
        }

        Bukkit.broadcastMessage(ChatColor.AQUA + cringe.getName() + ChatColor.GOLD + " is being pretty cringe rn.");
        return true;
    }
}
