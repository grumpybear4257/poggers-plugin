package io.gitlab.grumpybear4257.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Random;

import static io.gitlab.grumpybear4257.Utils.BukkitUtils.SendToAllPlayersWithPermission;

public class BeanCommand implements CommandExecutor {
    private static final Random rng = new Random();

    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if (!sender.hasPermission("ep.bean")) {
            sender.sendMessage(ChatColor.RED + "You do not have permission to use that command!");
            return true;
        }

        if (args.length != 1)
            return false;

        Player beaned = Bukkit.getPlayerExact(args[0]);
        if (beaned == null) {
            sender.sendMessage(ChatColor.RED + "Cannot find player with name: " + ChatColor.WHITE + args[0]);
            return true;
        }

        if (rng.nextInt(10000000) == 4257) {
            beaned.kickPlayer("lmao you actually got beaned by " + sender.getName() + " super unluck since it's 1 in 10 mill");
            SendToAllPlayersWithPermission("EP.bean.notify", ChatColor.AQUA + beaned.getName() + " just got beaned FR LMAO!");
        }

        Bukkit.broadcastMessage(ChatColor.YELLOW + beaned.getName() + " left the game");
        beaned.sendMessage(ChatColor.GOLD + "You just got beaned!");

        SendToAllPlayersWithPermission("EP.bean.notify", ChatColor.AQUA + beaned.getName() + " just got beaned!");
        return true;
    }
}
